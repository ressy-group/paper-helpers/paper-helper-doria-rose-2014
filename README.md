# Data Gathering from Doria-Rose 2014

Developmental pathway for potent V1V2-directed HIV-neutralizing antibodies.
Doria-Rose, N., Schramm, C., Gorman, J. et al.
Nature 509, 55–62 (2014).
<https://doi.org/10.1038/nature13036>

See also [paper-helper-doria-rose-2016](https://gitlab.com/ressy-group/paper-helpers/paper-helper-doria-rose-2016).

GenBank accessions:

 * KF996576 - KF996716  (env)
 * KJ133708 - KJ134387  (NGS heavy chains)
 * KJ134388 - KJ134859  (NGS light chains)
 * KJ134860 - KJ134889  (mAbs and inferences)

SRA accessions:

 * SRP017087: Illumina linked VH:VL antibody repertoire sequencing (but just SRR1056423 and 4?)
 * SRP034555: 454 antibody repertoire sequencing
   * two samples under SRX1117045: week 34 samples added later for the 2016 paper
   * all others: remaining 454 sequencing timepoints
