LOCUS       KF996661                2556 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.12mo.18 from South Africa envelope
            glycoprotein (env) gene, complete cds.
ACCESSION   KF996661
VERSION     KF996661.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2556)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2556)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2556
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.12mo.18"
                     /isolation_source="48 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="28-Jun-2006"
                     /note="subtype: C"
     gene            1..2556
                     /gene="env"
     CDS             1..2556
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ58976.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGLWVTVYYGVPVWKEA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCSDAKVNISATYNGTREEIKNCSFNATTELRDKI
                     KKEYALFYRLDIVPLSGEGNNNSEYRLINCNTSVITQACPKVTFDPIPIHYCAPAGYA
                     ILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEIIIRSENLTDNVK
                     TIIVHLNESIEINCTRPNNNTRKSIRIGPGQTFYATGDIIGDIRQAHCNISKIKWEKT
                     LQRVSKKLGEHFNKTIIFNQSSGGDLEITTHSFNCGGEFFYCNTSDLFFNKTFNETYS
                     TGSNSTNSTITLPCRIKQIINMWQEVGRAMYASPIAGEITCKSNITGLLLTRDGGGNN
                     STEETFRPGGGDMRNNWRSELYKYKVVEIKPLGIAPTEAKRRVVEREKRAVGMGAVIF
                     GFLGAAGSAMGAASMALTVQAKQLLSGIVQQQSNLLRAIEAQQHMLQLTVWGIKQLQA
                     RVLAIERYLKDQQLLGLWGCSGKLICTTAVPWNSSWSNKSQADIWDNMTWMQWDREIS
                     NYTGIIYSLLEESQNQQEKNEKDLLALDSWNSLWNWFSISTWLWYIRIFVMIVGGLIG
                     LRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEGGEQDRDRSIRLVSGF
                     FSLAWDDLRSLCLFCYHRLRDFILIAGRAVELLGRSSLQGLQRGWEILKYLGSLVQYW
                     GLELKKSAINLFDTLAIVVAEGTDRIIEFLQRIVRAILHIPRRIRQGFEAALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcttg tgggtcacag tctactatgg ggtacctgtg
      121 tggaaagaag caaaaactac tctattttgt gcctcagacg ctaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttggaaa atgtaacaga aaattttaac atgtggaaaa atgatatggt agatcagatg
      301 catgaagata taatcagttt atgggatcaa agcctcaagc catgtgtaaa gttgaccccg
      361 ctctgtgtca ctctaaactg tagcgatgca aaggtaaata taagtgctac ctataatgga
      421 acaagggaag aaataaaaaa ttgctctttc aatgcgacca cagaattaag agataagata
      481 aagaaagaat atgcactctt ttatagactt gatatagtac cacttagtgg ggagggtaat
      541 aacaacagtg aatatagatt aataaactgt aatacctcag tcataacaca agcctgtcca
      601 aaggtcactt ttgacccaat tcctatacat tattgtgctc cagctggtta tgcgattctc
      661 aagtgtaata ataagacatt caatggcaca ggaccatgca ataatgtcag cacagtacaa
      721 tgtacacatg gaattaagcc agtagtttca actcaactat tgttaaatgg tagcctagca
      781 gaagaagaga taataattag atcagaaaac ctgacagaca atgtcaaaac aataatagta
      841 catctcaatg aatctataga gattaattgt acaagaccca acaataatac aagaaaaagt
      901 ataagaatag gaccaggaca aacattctat gcaacaggag acataatagg agatataaga
      961 caagcacatt gtaacattag taaaattaaa tgggagaaaa ctttacaaag agtaagtaaa
     1021 aaattgggag aacacttcaa taagacaata atatttaatc aatcctcagg aggggaccta
     1081 gaaattacaa cacatagctt taattgtgga ggagaatttt tctattgcaa tacatcagat
     1141 ctgtttttta ataagacatt taatgagaca tatagtacag gaagtaattc aacaaattca
     1201 accatcacac tcccatgcag aataaagcaa attataaaca tgtggcagga ggtgggtcga
     1261 gcaatgtatg cctctcctat tgcaggagaa ataacatgta aatcaaatat cacaggacta
     1321 ctattgacac gtgatggagg aggaaacaac agtacagaag aaacattcag acctggagga
     1381 ggagatatga ggaacaattg gagaagtgaa ttatataaat ataaagtagt agaaattaaa
     1441 ccattaggaa tagcacccac tgaagcaaaa aggagagtgg tggagagaga gaaacgagca
     1501 gtaggaatgg gagctgtgat ctttgggttc ttgggagcag caggaagcgc tatgggcgcg
     1561 gcgtcaatgg cgctgacggt acaggccaaa caactgctgt ctggtatagt gcaacagcaa
     1621 agcaatttgc tgagagctat agaggcgcaa cagcatatgt tgcaactcac agtctggggc
     1681 atcaagcagc tccaggcaag agtcctggct atagaaagat acctaaaaga tcaacagctc
     1741 ctagggcttt ggggctgctc tggaaaactc atctgcacca ctgctgtacc ttggaattcc
     1801 agttggagta ataaatctca agcagatatt tgggataaca tgacctggat gcagtgggat
     1861 agagaaatta gtaattacac aggcataata tacagtttgc ttgaagaatc gcagaaccag
     1921 caggaaaaaa atgaaaaaga tttactagca ttggatagtt ggaacagtct gtggaattgg
     1981 tttagcatat caacatggct gtggtatata agaatattcg taatgatagt aggaggcttg
     2041 ataggtttaa gaataatttt tgctgtgctc tcgctagtga atagagttag gcagggatac
     2101 tcacctttgt catttcagac ccttacccca aacccgaggg aactcgacag gctcggagga
     2161 atcgaagaag aaggtggaga gcaagacaga gacagatcca tcagattagt gagcggattc
     2221 ttctcacttg cctgggacga cctgcggagc ctgtgcctct tctgctacca ccgattgaga
     2281 gacttcatat taattgcagg gagagcagtg gaacttctgg gacgcagcag tctccaggga
     2341 ctacagaggg ggtgggaaat ccttaagtac ctgggaagtc ttgtgcagta ttggggtcta
     2401 gagctaaaaa agagtgctat taatctgttt gataccttag caatagtagt agctgaagga
     2461 acagatagaa ttatagaatt cttacaaaga attgttagag ctatcctcca catacctaga
     2521 agaataagac agggctttga agcagctttg caataa
//
