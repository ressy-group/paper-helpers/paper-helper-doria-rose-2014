LOCUS       KF996627                2568 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.8mo.13 from South Africa envelope glycoprotein
            (env) gene, complete cds.
ACCESSION   KF996627
VERSION     KF996627.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2568)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2568)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2568
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.8mo.13"
                     /isolation_source="34 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="22-Mar-2006"
                     /note="subtype: C"
     gene            1..2568
                     /gene="env"
     CDS             1..2568
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ58942.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGLWVTVYYGVPVWREA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCTTAKGINSTDNANATKKPNEEIKNCSFNTITEV
                     RDKQKKEYALFYRLDIVPLSGEGNNNSEYRLINCNTSVITQACPKVTFDPIPIHYCAP
                     AGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEIIIRSENLT
                     DNVKTIIVHLNESVEINCIRPNNNTRKSVRIGPGQTFYATGDIIGDIRQAHCNISKIK
                     WEKTLQRVSEKLREHFNKTIIFNQSSGGDLEITTHSFNCGGEFFYCNTSDLFFNKTFN
                     ETYSTGSNSTNSTITLPCRIKQIINMWQEVGRAMYASPIAGEITCKSNITGLLLTRDG
                     GGNNSTEETFRPGGGDMRNNWRSELYKYKVVEIKPLGIAPTEAKRRVVEREKRAVGMG
                     AVIFGFLGAAGSTMGAASMALTVQAKQLLSGIVQQQSNLLRAIEAQQHMLQLTVWGIK
                     QLQARVLAIERYLKDQQLLGLWGCSGKLICTTAVPWNSSWSNKSQADIWDNMTWMQWD
                     REISNYTGIIYSLLEESQNQQEKNEKDLLALDSWNSLWNWFSISTWLWYIRIFVIIVG
                     GLIGLRIIFAVLSIVNRVRQGYSPLSFQTLTPNQRGLDRLGGIEEEGGEQDRDRSIRL
                     VSGFFSLAWDDLRSLCLFCYHRLRDFILIAGRAVELLGRSSLQGLQRGWEILKYLGSL
                     VQYWGLELKKSAINLFDTLAIVVAEGTDRIIEILQRIVRAILHIPRRIRQGFEAALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcttg tgggttacag tctactatgg ggtacctgtg
      121 tggagagaag caaaaactac tctattttgt gcctcagacg ctaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttggaaa atgtaacaga aaattttaac atgtggaaaa atgatatggt agatcagatg
      301 catgaagata taatcagttt atgggatcaa agcctcaagc catgtgtaaa attaacccca
      361 ctctgtgtca ctctaaattg tacaactgca aagggtatta atagtactga caatgcaaat
      421 gctactaaaa aaccaaatga agaaataaaa aattgctctt tcaatacaat cacagaggta
      481 agagataagc aaaagaaaga atatgcactc ttttatagac ttgatatagt accacttagt
      541 ggggagggta ataacaacag tgaatataga ttaataaact gtaatacctc agtcataaca
      601 caagcctgtc caaaggtcac ttttgaccca attcctatac attattgtgc tccagctggt
      661 tatgcgattc taaagtgcaa taacaagaca ttcaatggca caggaccatg caataatgtc
      721 agcacagtac aatgtacaca tggaattaag ccagtagttt caactcaact attgttaaat
      781 ggtagcctag cagaagaaga gataataatt agatcagaaa acctgacaga caatgtcaaa
      841 acaataatag tacatctcaa tgaatctgta gagattaatt gtataagacc caacaataat
      901 acaagaaaaa gtgtaagaat aggaccagga caaacattct atgcaacagg agacataata
      961 ggagatataa gacaagcaca ttgtaacatt agtaaaatta aatgggagaa aactttacaa
     1021 agagtaagtg aaaaattgag agaacacttc aataagacaa taatatttaa tcaatcctca
     1081 ggaggggacc tagaaattac aacacatagc tttaattgtg gaggagaatt tttctattgc
     1141 aatacatcag atctgttttt taataagaca tttaatgaga catatagtac aggaagtaat
     1201 tcaacaaatt caaccatcac actcccatgc agaataaagc aaattataaa catgtggcag
     1261 gaggtgggtc gagcaatgta tgcctctcct attgcaggag aaataacatg taaatcaaat
     1321 atcacaggac tactattgac acgtgatgga ggaggaaaca acagtacaga agaaacattc
     1381 agacctggag gaggagatat gaggaacaat tggagaagtg aattatataa atataaagta
     1441 gtagaaatta aaccattagg aatagcaccc actgaagcaa aaaggagagt ggtggagaga
     1501 gagaaacgag cagtaggaat gggagctgtg atctttgggt tcttgggagc agcaggaagc
     1561 actatgggcg cggcgtcaat ggcgctgacg gtacaggcca aacaactgct gtctggtata
     1621 gtgcaacagc aaagcaattt gctgagagct atagaggcgc aacagcatat gttgcaactc
     1681 acagtctggg gcatcaagca gctccaggca agagtcctgg ctatagaaag atacctaaaa
     1741 gatcaacagc tcctagggct ttggggctgc tctggaaaac tcatctgcac cactgctgta
     1801 ccttggaatt ccagttggag taataaatct caagcagata tttgggataa catgacctgg
     1861 atgcagtggg atagagaaat tagtaattac acaggcataa tatacagttt gcttgaagaa
     1921 tcgcagaacc agcaggaaaa aaatgaaaaa gatttactag cattggatag ttggaacagt
     1981 ctgtggaatt ggtttagcat atcaacatgg ctgtggtata taagaatatt cgtaataata
     2041 gtaggaggct tgataggttt aagaataatt tttgctgtgc tctctatagt gaatagagtt
     2101 aggcagggat actcaccttt gtcgtttcag acccttaccc caaaccagag gggactcgac
     2161 aggctcggag gaatcgaaga agaaggtgga gagcaagaca gagacagatc catcagatta
     2221 gtgagcggat tcttctcact tgcctgggac gacctgcgga gcctgtgcct cttctgctac
     2281 caccgattga gagacttcat attaattgca gggagagcag tggaacttct gggacgcagc
     2341 agtctccagg gactacaaag gggttgggaa atccttaagt atctgggaag tcttgtgcag
     2401 tattggggtc tagagctaaa aaagagtgct attaatctgt ttgatacctt agcaatagta
     2461 gtagctgaag gaacagatag aattatagaa atcttacaaa gaattgttag agctatcctc
     2521 cacataccta gaagaataag acagggcttt gaagcagctt tgcaataa
//
