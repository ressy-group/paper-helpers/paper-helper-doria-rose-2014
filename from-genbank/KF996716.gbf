LOCUS       KF996716                2568 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.39mo.11 from South Africa envelope
            glycoprotein (env) gene, complete cds.
ACCESSION   KF996716
VERSION     KF996716.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2568)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2568)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2568
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.39mo.11"
                     /isolation_source="176 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="09-Dec-2008"
                     /note="subtype: C"
     gene            1..2568
                     /gene="env"
     CDS             1..2568
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ59031.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGGLWVTVYYGVPVWRE
                     AKTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMH
                     EDIISLWDQSLKPCVKLTPLCVTLNCSDANRTVAYNGTREEIKNCSFNATTEVRDKEK
                     KEYALFYRLDLVSLNEGDSGNSNSSGNFSEYRLINCNTSVITQACPKVTFEPIPIHYC
                     APAGYAILKCNNKTFNGTGPCSNVSTVQCTHGIKPVVSTQLLLNGSLAEEEIIIRSEN
                     LTDNVKTIIVHLNESVEINCIRPNNNTRKSVRIGPGQTFYATGDIIGDIRQAHCNINK
                     TKWEETLQRVSEKLREHFNRTIIFNQSSGGDLEITTHSFNCRGEFFYCNTSGLFNKTF
                     NGTDSTGSNSTNSTITLPCRIKQIINMWQEVGRAMYAAPIAGEITCKSNITGLLLTRD
                     GGGNSNEETFRPGGGDMRNNWRSELYKYKVVEIKPLGIAPTDAKRRVVEREKRAVGMG
                     AVIFGFLGAAGSTMGAASMALTVQAKQLLSGIVQQQSNLLRAIEAQQHMLQLTVWGIK
                     QLQARVLAIERYLKDQQLLGLWGCSGKLICTTAVPWNSSWSNKSQADIWENMTWMQWE
                     REISNYTGIIYSLLEESQTQQEKNEKDLLALDSWNSLWNWFSISTWLWYIRIFVIIVG
                     GLIGLRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEGGEQDRGRSIRL
                     VNGFFSLVWEDLRSLCLFSYHHLRDFTLIAARTVELLGRSILKGLQRGWEVLKYLGSL
                     VQYWGLELKKSAISLLDTIAIAVAEGTDRIIEFLQRIVRAILHIPRRIRQGFEAALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcggc ttgtgggtta cagtctacta tggggtacct
      121 gtgtggagag aagcaaaaac tactctattt tgtgcctcag atgctaaatc atatgagaaa
      181 gaggtgcata atgtctgggc tacgcatgcc tgtgtaccca cagaccccaa cccacaagaa
      241 ctggttttgg aaaatgtaac agaaaatttt aacatgtgga aaaatgacat ggtggatcag
      301 atgcatgagg atataatcag tctatgggat caaagcctca agccatgtgt aaagttgacc
      361 ccgctctgtg tcactctaaa ctgtagcgat gcaaatagaa ctgttgccta taatggaaca
      421 agggaagaaa taaaaaattg ctctttcaat gcgaccacag aggtaagaga taaggaaaag
      481 aaagaatatg cactctttta tagacttgat ctagtatcac ttaatgaggg ggactctggg
      541 aactctaaca gctctggcaa ctttagtgaa tatagattaa taaactgtaa tacctcagtc
      601 ataacacaag cctgtccaaa ggtcactttt gaaccaattc ctatacatta ttgtgctcca
      661 gctggttatg cgattctaaa gtgtaataat aagacattca atggcacagg accatgcagt
      721 aatgtcagca cagtacaatg tacacatgga attaagccag tagtttcaac tcaactattg
      781 ttaaatggta gcctagcaga agaagagata ataattagat cagaaaacct gacagacaat
      841 gtcaaaacaa taatagtaca tctcaatgaa tctgtagaga ttaattgtat aagacccaac
      901 aataatacaa gaaaaagtgt aagaatagga ccaggacaaa cattctatgc aacaggagac
      961 ataataggag atataagaca agcacattgt aacattaata aaactaaatg ggaggaaact
     1021 ttacaaagag taagtgaaaa attgagagaa cacttcaata ggacaataat atttaatcaa
     1081 tcctcaggag gggacctaga aattacaaca catagcttta attgtagagg agaatttttc
     1141 tattgcaata catcaggtct gtttaataag acatttaatg ggacagatag tacaggaagt
     1201 aattcaacaa attcaaccat cacactccca tgcagaataa agcaaattat aaacatgtgg
     1261 caggaggtgg gtcgagcaat gtatgccgct cctattgcag gagaaataac atgtaaatca
     1321 aatatcacag gactactatt gacacgtgat ggaggaggaa acagcaatga agaaacattc
     1381 agacctggag gaggagatat gaggaacaat tggagaagtg aattatataa atataaagta
     1441 gtagaaatta aaccattagg aatagcaccc actgatgcaa aaaggagagt ggtggagaga
     1501 gagaaaagag cagtaggaat gggagctgtg atctttgggt tcttgggagc agcaggaagc
     1561 actatgggcg cggcgtcaat ggcgctgacg gtacaggcca aacaactgct gtctggtata
     1621 gtgcaacagc aaagcaattt gctgagagct atagaggcgc aacagcatat gttgcaactc
     1681 acagtctggg gcatcaagca gctccaggca agagtcctgg ctatagaaag atacctaaaa
     1741 gatcaacagc tcctagggct ttggggctgc tctggaaaac tcatctgcac cactgctgta
     1801 ccttggaatt ccagttggag taataaatct caagcagata tttgggagaa catgacctgg
     1861 atgcagtggg aaagagaaat tagtaattac acaggcataa tatacagttt gcttgaagaa
     1921 tcgcagaccc agcaggaaaa aaatgaaaag gatttactag cattggatag ttggaacagt
     1981 ctgtggaatt ggtttagcat atcaacatgg ctgtggtata taagaatatt cgtaataata
     2041 gtaggaggct tgataggttt aagaataatt tttgctgtgc tctcgctagt gaatagagtt
     2101 aggcagggat actcaccctt gtcatttcag acccttaccc caaacccgag ggaactcgac
     2161 aggctcggag gaatcgaaga agaaggtgga gagcaagaca gaggcagatc catcagatta
     2221 gtgaacggat tcttctcact tgtctgggaa gacctgcgga gcctgtgcct cttcagctac
     2281 caccacttga gagacttcac attgattgca gcgagaacag tggaacttct gggacgcagc
     2341 attctcaagg gactacagag ggggtgggaa gtccttaagt atctgggaag tcttgtgcag
     2401 tactggggtc tggaactaaa aaagagtgct attagtctgc ttgataccat agcaatagca
     2461 gtagctgaag gaacagatag gattatagaa ttcttacaaa gaattgttag agctatcctc
     2521 cacataccta gaagaataag acagggcttt gaagcagctt tgcaataa
//
