LOCUS       KF996630                2595 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.8mo.20 from South Africa envelope glycoprotein
            (env) gene, complete cds.
ACCESSION   KF996630
VERSION     KF996630.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2595)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2595)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2595
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.8mo.20"
                     /isolation_source="34 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="22-Mar-2006"
                     /note="subtype: C"
     gene            1..2595
                     /gene="env"
     CDS             1..2595
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ58945.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGLWVTVYYGVPVWKEA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCTTAKGINSTDNAKATKKPNEEIKNCSFNTITEV
                     RDKQKKEYALFYRLDLVSLNEGDAEGNSSRSGNFSTYRLINCNTSVITQACPKVTFDP
                     IPIHYCAPAGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEI
                     IIRSENLTDNVKTIIVHLNESVEINCIRPNNNTRKSVRIGPGQTFYATGDIIGDIRQA
                     HCNISKIKWEKTLQRVSEKLREHFNKTIIFNQSSGGDLEITTHSFNCGGEFFYCNTSD
                     LFFNKTFNETYSTGSNSTNSTITLPCRIKQIINMWQEVGRAMYASPIAGEITCKSNIT
                     GLLLTRDGGGNNSTEETFRPGGGNMRDNWRSELYKYKVVEVKPLGIAPTEARRRVVQR
                     ERRAVVGLGAVFLGFLGAAGSTMGAASITLTVQARQLLSGIVQQQSNLLRAIEAQQHM
                     LQLTVWGIKQLQARVLAIERYLKDQQLLGMWGCSGKLICTTNVYWNSSWSNKTYNEIW
                     DNMTWMQWDREIDNYTDTIYKLLEVSQKQQESNEKDLLALDSWNNLWNWFDISKWLWY
                     IKIFIMIVGGLIGLRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEGGE
                     QDRDRSIRLVSGFFSLAWDDLRSLCLFCYHRLRDFILIAGRAVELLGRSSLQGLQRGW
                     EILKYLGSLVQYWGLELKKSAINLFDTLAIVVAEGTDRIIEFLQRIVRAILHIPRRIR
                     QGFEAALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcttg tgggtcacag tctactatgg ggtacctgtg
      121 tggaaagaag caaaaactac tctattttgt gcctcagatg cgaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttggaaa atgtaacaga aaattttaac atgtggaaaa atgatatggt agatcagatg
      301 catgaagata taatcagttt atgggatcaa agcctcaagc catgtgtaaa attaacccca
      361 ctctgtgtca ctctaaattg tacaactgca aagggtatta atagtactga caatgcaaaa
      421 gctactaaaa aaccaaatga agaaataaaa aattgctctt tcaatacaat cacagaggta
      481 agagataagc aaaagaaaga atatgcactc ttttatagac ttgatctagt atcacttaat
      541 gagggggacg ctgaagggaa ctctagcaga tctggcaact ttagtacgta tagattaata
      601 aactgtaata cctcagtcat aacacaagcc tgtccaaagg tcacttttga cccaattcct
      661 atacattatt gtgctccagc tggttatgcg attctaaagt gtaataataa gacattcaat
      721 ggcacaggac catgcaataa tgtcagcaca gtacaatgta cacatggaat taagccagta
      781 gtttcaactc aactattgtt aaatggtagc ctagcagaag aagagataat aattagatct
      841 gaaaacctga cagacaatgt caaaacaata atagtacatc tcaatgaatc tgtagagatt
      901 aattgtataa gacccaacaa taatacaaga aaaagtgtaa gaataggacc aggacaaaca
      961 ttctatgcaa caggagacat aataggagat ataagacaag cacattgtaa cattagtaaa
     1021 attaaatggg agaaaacttt acaaagagta agtgaaaaat tgagagaaca cttcaataag
     1081 acaataatat ttaatcaatc ctcaggaggg gacctagaaa ttacaacaca tagctttaat
     1141 tgtggaggag aatttttcta ttgcaataca tcagatctgt tttttaataa gacatttaat
     1201 gagacatata gtacaggaag taattcaaca aattcaacca tcacactccc atgcagaata
     1261 aagcaaatta taaacatgtg gcaggaggtg ggtcgagcaa tgtatgcctc tcctattgca
     1321 ggagaaataa catgtaaatc aaatatcaca ggactactat tgacacgtga tggaggagga
     1381 aacaacagta cagaagagac attcagacct ggaggaggaa atatgaggga caactggaga
     1441 agtgaattat ataaatataa agtggtagaa gttaagccat taggaatagc acccactgaa
     1501 gcaagaagga gagtggtgca gagagagaga agagcagtag tgggattagg agctgtgttc
     1561 cttgggttct tgggagcagc aggaagcact atgggcgcag catcaataac gctgacggta
     1621 caggccagac aattattgtc tggtatagtg caacagcaaa gcaatttgct gagggctata
     1681 gaggcgcaac agcatatgtt gcaactcacg gtctggggca ttaagcagct ccaggcaaga
     1741 gtcctggcca tagaaagata cctaaaggat caacagctcc tagggatgtg gggatgctct
     1801 ggaaaactca tctgcaccac taatgtgtat tggaactcta gttggagtaa taaaacttac
     1861 aatgaaattt gggataacat gacatggatg cagtgggata gagaaattga taactacaca
     1921 gacacaatat acaagctgct tgaagtctcg caaaaacagc aggagagcaa tgaaaaagat
     1981 ttactagcat tggacagttg gaacaatctg tggaattggt ttgacatatc aaagtggctg
     2041 tggtatataa aaatattcat aatgatagta ggaggcttaa taggtttaag aataattttt
     2101 gctgtgctct cgctagtgaa tagagttagg cagggatact cacctttgtc atttcagacc
     2161 cttaccccaa acccgaggga actcgacagg ctcggaggaa tcgaagaaga aggtggagag
     2221 caagacagag acagatccat cagattagtg agcggattct tctcacttgc ctgggacgac
     2281 ctgcggagcc tgtgcctctt ctgctaccac cgattgagag acttcatatt aattgcaggg
     2341 agagcagtgg aacttctggg acgcagcagt ctccagggac tacagagggg gtgggaaatc
     2401 cttaagtacc tgggaagtct tgtgcagtat tggggtctag agctaaaaaa gagtgctatt
     2461 aatctgtttg ataccttagc aatagtagta gctgaaggaa cagatagaat tatagaattc
     2521 ttacaaagaa ttgttagagc tatcctccac atacctagaa gaataagaca gggctttgaa
     2581 gcagctttgc aataa
//
