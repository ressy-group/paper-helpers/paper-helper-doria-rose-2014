LOCUS       KF996705                2571 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.39mo.F10 from South Africa envelope
            glycoprotein (env) gene, complete cds.
ACCESSION   KF996705
VERSION     KF996705.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2571)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2571)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2571
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.39mo.F10"
                     /isolation_source="176 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="09-Dec-2008"
                     /note="subtype: C"
     gene            1..2571
                     /gene="env"
     CDS             1..2571
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ59020.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWKLMICNGLWVTVYYGVPVWREA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCTTAKDSNSTDHANVTTNEEIKNCSFNTITEVSD
                     KQKNEYALFYRLDLVSLNEGDSEGNSSNFRTYRLINCNTSVITQACPKVTFEPIPIHY
                     CAPAGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEIIIRSE
                     NLTDNVKTIIVHLNESVEINCIRPNNNTRKSVRIGPGQTFYATGDIIGDIRQAHCNIN
                     KTKWEKTLQRVSEKLREHFNKTIIFNQSSGGDLEITTHSFNCRGEFFYCNTSGLFNKT
                     FNGTDSTGSNSTNSTITFPCRIKQIINMWQEVGRAMYASPIAGEITCKSNITGLLLTR
                     DGGGNSEEETFRPGGGDMRNNWRSELYKYKVVEIKPLGIAPTEAKRRVVEREKRAVGM
                     GAVIFGFLGAAGSTMGAASITLTVQARQLLSGIVQQQSNLLRAIEAQQHMLQLTVWGI
                     KQLQARVLAIERYLKDQQLLGLWGCSGKLICTTAVPWNSSWSNKSQADIWENMTWMQW
                     EREISNYTGIIYSLLEESQNQQEKNEKDLLALDSWNSLWNWFSISTWLWYIRIFVIIV
                     GGLIGLRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEGGEQDRGRSIR
                     LVNGFLALVWDDLRSLCLFSYHHLRDFTLIAARAVELLGRSILKGLQRGWEILKYLGS
                     LVQYWGLELKRIAISLLDTIAIAVAEGTDRIIEFLQRIVRAILHIPRRIRQGFEAALQ
                     "
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggaagt taatgatttg taatggcttg tgggtcacag tctactatgg ggtacctgtg
      121 tggagagaag caaaaactac tctattttgt gcctcagatg ctaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttggaaa atgtaacaga aaattttaac atgtggaaaa atgacatggt ggatcagatg
      301 catgaggata taatcagtct atgggatcaa agcctcaaac catgtgtaaa attaacccca
      361 ctctgtgtca ctctaaattg tacaactgca aaagatagta atagtactga ccatgcaaat
      421 gttactacaa atgaagaaat aaaaaattgc tctttcaata ccatcacaga ggtaagtgat
      481 aagcaaaaga atgaatatgc actcttttat agacttgatc tagtatcact taatgagggg
      541 gactctgaag ggaactctag caactttagg acgtatagat taataaactg taatacctca
      601 gtcataacac aagcctgtcc aaaggtcact tttgaaccaa ttcctataca ttattgtgct
      661 ccagctggtt atgcgattct aaagtgtaat aataagacat tcaatggcac aggaccatgc
      721 aataatgtca gcacagtaca atgtacacat ggaattaagc cagtagtttc aactcaacta
      781 ttgttaaatg gtagcctagc agaagaagag ataataatta gatcagaaaa cctgacagac
      841 aatgtcaaaa caataatagt acatctcaat gaatctgtag agattaattg tataagaccc
      901 aacaataata caagaaaaag tgtaagaata ggaccaggac aaacattcta tgcaacagga
      961 gacataatag gagatataag acaagcacat tgtaacatta ataaaactaa atgggagaaa
     1021 actttacaaa gagtaagtga aaaattgaga gaacacttca ataagacaat aatatttaat
     1081 caatcctcag gaggggacct agaaattaca acacatagct ttaattgtag aggagaattt
     1141 ttctattgca atacatcagg tctgtttaat aagacattta atgggacaga tagtacagga
     1201 agtaattcaa caaattcaac catcacattc ccatgcagaa taaaacaaat tataaacatg
     1261 tggcaggagg tgggtcgagc aatgtatgcc tctcctattg caggagaaat aacatgtaaa
     1321 tcaaatatca caggactact attgacacgt gatggaggag gaaacagtga agaagaaaca
     1381 ttcagacctg gaggaggaga tatgaggaac aattggagaa gtgaattata taaatataaa
     1441 gtagtagaaa ttaaaccatt aggaatagca cccactgaag caaaaaggag agtggtggag
     1501 agagagaaac gagcagtagg aatgggagct gtgatctttg ggttcttggg agcagcagga
     1561 agcactatgg gcgcagcatc aataacgctg acggtacagg ccagacaatt attgtctggt
     1621 atagtgcaac agcaaagcaa tttgctgaga gctatagagg cgcaacagca tatgttgcaa
     1681 ctcacagtct ggggcatcaa gcagctccag gcaagagtcc tggctataga aagataccta
     1741 aaagatcaac agctcctagg gctttggggc tgctctggaa aactcatctg caccactgct
     1801 gtaccttgga attccagttg gagtaataaa tctcaagcag atatttggga gaacatgacc
     1861 tggatgcagt gggaaagaga aattagtaat tacacaggca taatatacag tttgcttgaa
     1921 gaatcgcaga accagcagga aaaaaatgaa aaagatttac tagcattgga tagttggaac
     1981 agtctgtgga attggtttag catatcaaca tggctgtggt atataagaat atttgtaata
     2041 atagtaggag gcttgatagg tttaagaata atttttgctg tgctctcgct agtgaataga
     2101 gttaggcagg gatactcacc tttgtcattt cagaccctta ccccaaaccc gagggaactc
     2161 gacaggctcg gaggaatcga agaagaaggt ggagagcaag acagaggcag atccatacga
     2221 ttagtgaacg gattcttagc acttgtctgg gacgacctgc ggagcctgtg cctcttcagt
     2281 taccaccact tgagagactt cacattgatt gcagcgagag cagtggaact tctgggacgc
     2341 agcattctca agggactaca gagggggtgg gaaatcctta agtacctggg aagtcttgtg
     2401 cagtattggg gtctagagct aaaaaggatt gctattagtc tgcttgatac catagcaata
     2461 gcagtagctg aaggaacaga taggattata gaattcttac aaagaattgt tagagctatc
     2521 ctccatatac ctagaagaat aagacagggc tttgaagcag ctttgcaata a
//
