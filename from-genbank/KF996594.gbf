LOCUS       KF996594                2598 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.5mo.22 from South Africa envelope glycoprotein
            (env) gene, complete cds.
ACCESSION   KF996594
VERSION     KF996594.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2598)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2598)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2598
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.5mo.22"
                     /isolation_source="23 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="05-Jan-2006"
                     /note="subtype: C"
     gene            1..2598
                     /gene="env"
     CDS             1..2598
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ58909.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGLWVTVYYGVPVWKEA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLKNVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCTTAKGINSTDNANATKKPNEEIKNCSFNTITEV
                     RDKQKKEYALFYRLDLVSLNEGDSEGNSSRSGNFSTYRLINCNTSVITQACPKVTFDP
                     IPIHYCAPAGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEI
                     IIRSENLTDNVKTIIVHLNESVEINCTRPNNNTRKSIRIGPGQTFYATGDIIGDIRQA
                     HCNISEIKWEKTLQRVSEKLREHFNKTIIFNQSSGGDLEITTHSFNCGGEFFYCNTSD
                     LFFNKTFNETYSTGSNSTNSTITLPCRIKQIINMWQEVGRAMYASPIAGEITCKSNIT
                     GLLLTRDGGGNNSTETETFRPGGGNMRDNWRSELYKYKVVEVKPLGIAPTEARRRVVQ
                     RERRAVVGLGAVFLGFLGAAGSTMGAASITLTVQARQLLSGIVQQQSNLLRAIEAQQH
                     MLQLTVWGIKQLQARVLAIERYLKDQQLLGMWGCSGKLICTTNVYWNSSWSNKTYNEI
                     WDNMTWMQWDREIDNYTDTIYKLLEVSQKQQESNEKDLLALDSWNNLWNWFDISKWLW
                     YIKIFIMIVGGLIGLRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEGG
                     EQDRDRSIRLVSGFFSLAWDDLRSLCLFCYHRLRDFILIAGRAVELLGRSSLQGLQRG
                     WEILKYLGSLVQYWGLELKKSAINLFDTLAIVVAEGTDRIIEFLQRIVRAILHIPRRI
                     RQGFEAALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcttg tgggtcacag tctactatgg ggtacctgtg
      121 tggaaagaag caaaaactac tctattttgt gcctcagatg ctaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttgaaaa atgtaacaga aaattttaac atgtggaaaa atgatatggt agatcagatg
      301 catgaagata taatcagttt atgggatcaa agcctcaagc catgtgtaaa attaacccca
      361 ctctgtgtca ctctaaattg tacaactgca aagggtatta atagtactga caatgcaaat
      421 gctactaaaa aaccaaatga agaaataaaa aattgctctt tcaatacaat cacagaggta
      481 agagataagc aaaagaaaga atatgcactc ttttatagac ttgatctagt atcacttaat
      541 gagggggact ctgaagggaa ctctagcaga tctggcaact ttagtacgta tagattaata
      601 aactgtaata cctcagtcat aacacaagcc tgtccaaagg tcacttttga cccaattcct
      661 atacattatt gtgctccagc tggttatgcg attctaaagt gtaataataa gacattcaat
      721 ggcacaggac catgcaataa tgtcagcaca gtacaatgta cacatggaat taagccagta
      781 gtttcaacac aactattgtt aaatggtagc ctagcagaag aagagataat aattagatct
      841 gaaaacctga cagacaatgt caaaacaata atagtacatc tcaatgaatc tgtagagatt
      901 aattgtacaa gacccaacaa taatacaaga aaaagtataa gaataggacc aggacaaaca
      961 ttctatgcaa caggagacat aataggagat ataagacaag cacattgtaa cattagtgaa
     1021 attaaatggg agaaaacttt acaaagagta agtgaaaaat tgagagaaca cttcaataag
     1081 acaataatat ttaatcaatc ctcaggaggg gacctagaaa ttacaacaca tagctttaat
     1141 tgtggaggag aatttttcta ttgcaataca tcagatctgt tttttaataa gacatttaat
     1201 gagacatata gtacaggaag taattcaaca aattcaacca tcacactccc atgcagaata
     1261 aagcaaatta taaacatgtg gcaggaggtg ggtcgagcaa tgtatgcctc tcctattgca
     1321 ggagaaataa catgtaaatc aaatatcaca ggactactat tgacacgtga tggaggagga
     1381 aacaacagta cagagacaga gacattcaga cctggaggag gaaatatgag ggacaactgg
     1441 agaagtgaat tatataaata taaagtggta gaagttaagc cattaggaat agcacccact
     1501 gaagcaagaa ggagagtggt gcagagagag agaagagcag tagtgggatt aggagctgtg
     1561 ttccttgggt tcttgggagc agcaggaagc actatgggcg cagcatcaat aacgctgacg
     1621 gtacaggcca gacaattatt gtctggtata gtgcaacagc aaagcaattt gctgagggct
     1681 atagaggcgc aacagcatat gttgcaactc acggtctggg gcattaagca gctccaggca
     1741 agagtcctgg ccatagaaag atacctaaag gatcaacagc tcctagggat gtggggatgc
     1801 tctggaaaac tcatctgcac cactaatgtg tattggaact ctagttggag taataaaact
     1861 tacaatgaaa tttgggataa catgacatgg atgcagtggg atagagaaat tgataactac
     1921 acagacacaa tatacaagct gcttgaagtc tcgcaaaaac agcaggagag caatgaaaaa
     1981 gatttactag cattggacag ttggaacaat ctgtggaatt ggtttgacat atcaaaatgg
     2041 ctgtggtata taaaaatatt cataatgata gtaggaggct tgataggctt aagaataatt
     2101 tttgctgtgc tctcgctagt gaatagagtt aggcagggat actcaccttt gtcatttcag
     2161 acccttaccc caaacccgag ggaactcgac aggctcggag gaatcgaaga agaaggtgga
     2221 gagcaagaca gagacagatc cataagatta gtgagcggat tcttctcact tgcctgggac
     2281 gacctgcgga gcctgtgcct cttctgctac caccgattga gagacttcat attaattgca
     2341 gggagagcag tggaacttct gggacgcagc agtctccagg gactacaaag gggttgggaa
     2401 atccttaagt atctgggaag tcttgtgcag tattggggtc tagagctaaa aaagagtgct
     2461 attaatctgt ttgatacctt agcaatagta gtagctgaag gaacagatag aattatagaa
     2521 ttcttacaaa gaattgttag agctatcctc cacataccta gaagaataag acagggcttt
     2581 gaagcagctt tgcaataa
//
