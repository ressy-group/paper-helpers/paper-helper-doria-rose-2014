LOCUS       KF996682                2577 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.14mo.6a from South Africa envelope
            glycoprotein (env) gene, complete cds.
ACCESSION   KF996682
VERSION     KF996682.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2577)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2577)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2577
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.14mo.6a"
                     /isolation_source="59 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="14-Sep-2006"
                     /note="subtype: C"
     gene            1..2577
                     /gene="env"
     CDS             1..2577
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ58997.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGLWVTVYYGVPVWKEA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCTTINSTDTANATKKPNEEIKNCSFTTITEVRDK
                     QKKEYALFYRLDLVSLNEGESEGNSNRSGNFSTYRLINCNTSVITQACPKVTFEPIPI
                     HYCAPAGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEIIIR
                     SENLTDNVKTIIVHLNESVEIICTRPNNNTRKSVRIGPGQTFYATGDIIGDIRQAHCN
                     ISKIKWEKTLQRVSGKLREHFNKTIIFNQSSGGDLEITTHSFNCGGEFFYCNTSDLFF
                     NKTFNGTYSTGSNSTITLPCRIKQIINMWQEVGRAMYASPIAGEITCKSNITGLLLTR
                     DGGGNNSTEETFRPGGGNMRDNWRSELYKYKVVEVKPLGIAPTEARRRVVQRERRAVV
                     GLGAVFLGFLGAAGSTMGAASITLTVQARQLLSGIVQQQSNLLRAIEAQQHMLQLTVW
                     GIKQLQARVLAIERYLKDQQLLGMWGCSGKLICTTNVYWNSSWSNKTYNEIWDNMTWM
                     QWDREISNYTGIIYSLLEESQNQQEKNEKDLLALDSWNNLWNWFDISKWLWYIKIFIM
                     IVGGLIGLRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEGGEQDRDRS
                     IRLVNGFLALVWDDLRSLCLFSYHHLRDFTLIAARAVELLGRSILKGLQRGWEALKYL
                     GSLVQYWGLELKGSAISLLDTIAIAVAEGTDRIIEFLQRIVRAILHIPRRIRQGFEAA
                     LQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcttg tgggtcacag tctactatgg ggtacctgtg
      121 tggaaagaag caaaaactac tctattttgt gcctcagatg ctaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttggaaa atgtaacaga aaattttaac atgtggaaaa atgacatggt ggatcagatg
      301 catgaggata taatcagtct atgggatcaa agcctcaaac catgtgtaaa attaacccca
      361 ctctgtgtca ccctaaattg tacaactatt aatagtactg acactgcaaa tgctactaaa
      421 aaaccaaatg aagaaataaa aaattgctct ttcactacaa tcacagaggt aagagataag
      481 caaaagaaag aatatgcact cttttataga cttgatctag tatcacttaa tgagggggaa
      541 tctgaaggga actctaacag atctggcaac tttagtacgt atagattaat aaactgtaat
      601 acctcagtca taacacaagc ctgtccaaag gtcacttttg aaccaattcc tatacattat
      661 tgtgctccag ctggttatgc gattctaaag tgtaataata agacattcaa tggcacagga
      721 ccatgcaata atgtcagcac agtacaatgt acacatggaa ttaagccagt agtttcaact
      781 caactattgt taaatggtag cctagcagaa gaagagataa taattagatc tgaaaacctg
      841 acagacaatg tcaaaacaat aatagtacat ctcaatgaat ctgtagagat tatttgtaca
      901 agacccaaca ataatacaag aaaaagtgta agaataggac caggacaaac attctatgca
      961 acaggagaca taataggaga tataagacaa gcacattgta acattagtaa aattaagtgg
     1021 gagaaaactt tacaaagagt aagtggaaaa ttgagagaac acttcaataa gacaataata
     1081 tttaatcaat cctcaggagg ggacctagaa attacaacac atagctttaa ttgtggagga
     1141 gaatttttct attgcaatac atcagatctg ttttttaata agacatttaa tgggacatat
     1201 agtacaggaa gtaattcaac catcacactc ccatgcagaa taaagcaaat tataaacatg
     1261 tggcaggaag tgggtcgagc aatgtatgcc tctcctattg caggagaaat aacatgtaaa
     1321 tcaaatatca caggactact attgacacgt gatggaggag gaaacaacag tacagaagag
     1381 acattcagac ctggaggagg aaatatgagg gacaactgga gaagtgaatt atataaatat
     1441 aaagtggtag aagttaagcc attaggaata gcacccactg aagcaagaag gagagtggtg
     1501 cagagagaga gaagagcagt agtgggatta ggagctgtgt tccttgggtt cttgggagca
     1561 gcaggaagca ctatgggcgc agcatcaata acgctgacgg tacaggccag acaattattg
     1621 tctggtatag tgcaacagca aagcaatttg ctgagggcta tagaggcgca acagcatatg
     1681 ttgcaactca cggtctgggg cattaagcag ctccaggcaa gagtcctggc catagaaaga
     1741 tacctaaagg atcaacagct cctagggatg tggggatgct ctggaaaact catctgcacc
     1801 actaatgtgt attggaactc tagttggagt aataaaactt acaatgaaat ttgggataac
     1861 atgacctgga tgcagtggga tagagaaatt agtaattaca caggcataat atacagtttg
     1921 cttgaagaat cgcagaacca gcaggaaaaa aatgaaaaag atttactagc attggacagt
     1981 tggaacaatc tgtggaattg gtttgacata tcaaaatggc tgtggtatat aaaaatattc
     2041 ataatgatag taggaggctt gataggctta agaataattt ttgctgtgct ctcgctagtg
     2101 aatagagtta ggcagggata ctcacctttg tcatttcaga cccttacccc aaacccgagg
     2161 gaactcgaca ggctcggagg aatcgaagaa gaaggtggag agcaagacag agacagatcc
     2221 atcagattag tgaacggatt cttagcactt gtctgggacg acctgcggag cctgtgcctc
     2281 ttcagctacc accacttgag agacttcaca ttgattgcag cgagagcagt ggaacttctg
     2341 ggacgcagca ttctcaaggg actacagagg gggtgggaag cccttaagta tctgggaagt
     2401 cttgtgcagt actggggtct ggaactaaaa gggagtgcta ttagtctgct tgataccata
     2461 gcaatagcag tagctgaagg aacagataga attatagaat tcttacaaag aattgttaga
     2521 gctatcctcc acatacctag aagaataaga cagggctttg aagcagcttt gcaataa
//
