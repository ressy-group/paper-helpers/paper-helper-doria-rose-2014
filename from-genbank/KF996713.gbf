LOCUS       KF996713                2601 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.39mo.4 from South Africa envelope glycoprotein
            (env) gene, complete cds.
ACCESSION   KF996713
VERSION     KF996713.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2601)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2601)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2601
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.39mo.4"
                     /isolation_source="176 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="09-Dec-2008"
                     /note="subtype: C"
     gene            1..2601
                     /gene="env"
     CDS             1..2601
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ59028.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGDLWVTVYYGVPVWRE
                     AKTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMH
                     EDIISLWDQSLKPCVKLTPLCVTLNCTHINSTTINSTDTVNATYNGAREEIKNCSFNA
                     TTEVRDKEKKEYALFYRLDLVSLNEGDSGNSNRSGNFSEYRLINCNTSVITQACPKVT
                     FEPIPIHYCAPAGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAE
                     EEIIIRSENLTDNVKTIIVHLNESVEINCIRPNNNTRKSVRIGPGQTFYATGDIIGDI
                     RQAHCNINKTKWEKTLQRVSEKLREHFNKTIIFNQSSGGDLEITTHSFNCRGEFFYCN
                     TSGLFNKTFNGTDSTGSNSTNSTITLPCRIKQIINMWQEVGRAMYAAPIAGEITCKSN
                     ITGLLLTRDGGGNSSEETETFRPGGGDMRNNWRSELYKYKVVEIKPLGIAPTDAKRRV
                     VEREKRAVGMGAVIFGFLGAAGSTMGAASIALTVQAKQLLSGIVQQQSNLLRAIEAQQ
                     HMLQLTVWGIKQLQARVLAIERYLKDQQLLGLWGCSGKLICTTAVPWNSSWSNKSQAD
                     IWENMTWMQWEREISNYTGIIYSLLEESQNQQEKNEKDLLALDSWNSLWNWFSISTWL
                     WYIRIFVIIVGGLIGLRIIFAVFSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEG
                     GEQDRGRSIRLVSGFFSLVWEDLRSLCLFSYHHLRDFTLIAARAVELLGRSILKGLQR
                     GWEALKYLGSLVQYWGLELKKSAISLLDTIAIAVAEGTDRIIEFLQRIVRAILHIPRR
                     IRQGFEAALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcgac ttgtgggtta cagtctacta tggggtacct
      121 gtgtggagag aagcaaaaac tactctattt tgtgcctcag atgctaaatc atatgagaaa
      181 gaggtgcata atgtctgggc tacacatgcc tgtgtaccca cagaccccaa cccacaagaa
      241 ttggttttgg aaaatgtaac agaaaatttt aacatgtgga aaaatgacat ggtggatcag
      301 atgcatgagg atataatcag tttatgggat caaagcctca aaccatgtgt aaaattaacc
      361 ccactctgtg tcaccctaaa ttgtacacat attaatagta caactattaa tagtactgac
      421 actgtaaatg ctacctataa tggagcaagg gaagaaataa aaaattgctc tttcaatgcg
      481 accacagagg taagagataa ggaaaagaaa gaatatgcac tcttttatag acttgatcta
      541 gtatcactta atgaggggga ctctgggaac tctaacagat ctggcaactt tagtgaatat
      601 agattaataa actgtaatac ctcagtcata acacaagcct gtccaaaggt cacttttgaa
      661 ccaattccta tacattattg tgctccagct ggttatgcga ttctaaagtg taataataag
      721 acattcaatg gcacaggacc atgcaataat gtcagcacag tacaatgtac acatggaatt
      781 aagccagtag tttcaactca actattgtta aatggtagcc tagcagaaga agagataata
      841 attagatcag aaaacctgac agacaatgtc aaaacaataa tagtacatct caatgaatct
      901 gtagagatta attgtataag acccaacaat aatacaagaa aaagtgtaag aataggacca
      961 ggacaaacat tctatgcaac aggagacata ataggagata taagacaagc acactgtaac
     1021 attaataaaa ctaaatggga gaaaacttta caaagagtaa gtgaaaaatt gagagaacac
     1081 ttcaataaga caataatatt taatcaatcc tcaggagggg acctagaaat tacaacacat
     1141 agctttaatt gtagaggaga atttttctat tgcaatacat caggtctgtt taataagaca
     1201 tttaatggga cagatagtac aggaagtaat tcaacaaatt caaccatcac actcccatgc
     1261 agaataaagc aaattataaa catgtggcag gaggtgggtc gagcaatgta tgccgctcct
     1321 attgcaggag aaataacatg taaatcaaat atcacaggac tactattgac acgtgatgga
     1381 ggaggaaaca gcagtgaaga aacagaaaca ttcagacctg gaggaggaga tatgaggaac
     1441 aattggagaa gtgaattata taaatataaa gtagtagaaa ttaaaccatt aggaatagca
     1501 cccactgatg caaaaaggag agtggtggag agagagaaac gagcagtagg aatgggagct
     1561 gtgatctttg ggttcttggg agcagcagga agcactatgg gcgcggcgtc aatagcgctg
     1621 acggtacagg ccaaacaact gctgtctggt atagtgcaac agcaaagcaa tttgctgaga
     1681 gctatagagg cgcaacagca tatgttgcaa ctcacagtct ggggcatcaa gcagctccag
     1741 gcaagagtcc tggctataga aagataccta aaagatcaac agctcctagg gctttggggc
     1801 tgctctggaa aactcatctg caccactgct gtaccttgga attccagttg gagtaataaa
     1861 tctcaagcag atatttggga gaacatgacc tggatgcagt gggaaagaga aattagtaat
     1921 tacacaggca taatatacag tttgcttgaa gaatcgcaga accagcagga aaaaaatgaa
     1981 aaagatttac tagcattgga tagttggaac agtctgtgga attggtttag catatcaaca
     2041 tggctgtggt atataagaat attcgtaata atagtaggag gcttgatagg tttaagaata
     2101 atttttgctg tgttctcgct agtgaataga gttaggcagg gatactcacc tctgtcattt
     2161 cagaccctta ccccaaaccc gagggaactc gacaggctcg gaggaatcga agaagaaggt
     2221 ggagagcaag acagaggcag atccatcaga ttagtgagcg gattcttctc acttgtctgg
     2281 gaagacctgc ggagcctgtg cctcttcagc taccaccact tgagagactt cacattgatt
     2341 gcagcgagag cagtggaact tctgggacgc agcattctca aggggctaca gagggggtgg
     2401 gaagccctta agtatctggg aagtcttgtg cagtactggg gtctggaact aaaaaagagt
     2461 gctattagtc tgcttgatac catagcaata gcagtagctg aaggaacaga taggattata
     2521 gaattcttac aaagaattgt tagagctatc ctccacatac ctagaagaat aagacagggc
     2581 tttgaagcag ctttgcaata a
//
