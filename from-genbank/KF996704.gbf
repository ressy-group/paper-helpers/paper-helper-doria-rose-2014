LOCUS       KF996704                2580 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.39mo.H1 from South Africa envelope
            glycoprotein (env) gene, complete cds.
ACCESSION   KF996704
VERSION     KF996704.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2580)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2580)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2580
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.39mo.H1"
                     /isolation_source="176 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="09-Dec-2008"
                     /note="subtype: C"
     gene            1..2580
                     /gene="env"
     CDS             1..2580
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ59019.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGLWVTVYYGVPVWKEA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLENVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCTTAKDINSTDHVNVTTNEEIKNCSFNTITEVSD
                     KQKNEYALFYRLDLVSLNEGDSDGNSSNFSMYRLINCNTSVITQACPKVTFDPIPIHY
                     CAPAGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEIIIRSE
                     NLTDNVKTIIVHLNESVGINCTRPNNNTRKSIRIGPGQTFYATGDIIGDIRQAYCTIS
                     KIKWEETLQRVSEKLREHFNKTIIFNQSSGGDLEITTHSFNCRGEFFYCNTSDLFFNK
                     TFSGTDSTGSNSTNSTITLPCRIKQIINMWQEVGRAMYASPIAGEITCKSNITGLLLT
                     RDGGGNNSSAEETFRPGGGDMRNNWRSELYKYKVVEIKPLGIAPTEAKRRVVEREKRA
                     VGMGAVIFGFLGAAGSTMGAASITLTVQARQLLSGIVQQQSNLLRAIEAQQHMLQLTV
                     WGIKQLQARVLAIERYLKDQQLLGLWGCSGKLICTTAVPWNSSWSNKSQADIWDNMTW
                     MQWDREISNYTGIIYSLLEESQNQQEKNEKDLLALDSWNSLWNWFSISTWLWYIRIFV
                     IIVGGLIGLRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRLGGIEEEGGEQDRGR
                     SIRLVNGFLALVWDDLRSLCLFSYHHLRDFTLIAARAVELLGRSILKGLQRGWEILKY
                     LGSLVQSWGLELKKSAISLLDTIAIAVAEGTDRIIEFLQRIVRAILHIPRRIRQGFEA
                     ALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcttg tgggtcacag tctactatgg ggtacctgtg
      121 tggaaagaag caaaaactac tytattttgt gcctcagatg ctaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttggaaa acgtaacaga aaattttaac atgtggaaaa atgacatggt ggatcagatg
      301 catgaggata taatcagtct atgggatcaa agcctcaaac catgtgtaaa attaacccca
      361 ctctgtgtca ctctaaattg tacaactgca aaagatatta atagtactga ccatgtaaat
      421 gttactacaa atgaagaaat aaaaaattgc tctttcaata ccatcacaga ggtaagtgat
      481 aagcaaaaga atgaatatgc actcttttat agacttgatc tagtatcact taatgagggg
      541 gactctgacg ggaactctag caactttagt atgtatagat taataaactg taatacctca
      601 gtcataacac aagcctgtcc aaaggtcact tttgacccaa ttcctataca ttattgtgct
      661 ccagctggtt atgcgattct aaagtgtaat aataagacat tcaatggcac aggaccatgc
      721 aataatgtca gcacagtaca atgtacacat ggaattaagc cagtagtttc aactcaacta
      781 ttgttaaatg gtagcctagc agaagaagag ataataatta gatctgaaaa cctgacagac
      841 aatgtcaaaa caataatagt acatctcaat gaatctgtag ggattaattg tacaagaccc
      901 aacaataata caagaaaaag tataagaata ggaccaggac aaacattcta tgcaacagga
      961 gacataatag gagatataag acaagcatat tgtaccatta gtaaaattaa atgggaggaa
     1021 actttacaaa gagtaagtga aaaattgaga gaacacttca ataagacaat aatatttaat
     1081 caatcctcag gaggggacct agaaattaca acacacagct ttaattgtag aggagaattt
     1141 ttctattgca atacatcaga tctgtttttt aataagacat ttagtgggac agatagtaca
     1201 ggaagtaatt caacaaattc aaccatcaca ctcccatgca gaataaagca aattataaac
     1261 atgtggcagg aggtgggtcg agcaatgtat gcctctccta ttgcaggaga aataacatgt
     1321 aaatcaaata tcacaggact actattgaca cgtgatggag gaggaaacaa cagtagtgca
     1381 gaagaaacat tcagacctgg aggaggagat atgaggaaca attggagaag tgaattatat
     1441 aaatataaag tagtagaaat taaaccatta ggaatagcac ccactgaagc aaaaaggaga
     1501 gtggtggaga gagagaaacg agcagtagga atgggagctg tgatctttgg gttcttgggc
     1561 gcagcaggaa gcactatggg cgcagcatca ataacgctga cggtacaggc cagacaatta
     1621 ttgtctggta tagtgcaaca gcaaagcaat ttgctgagag ctatagaggc gcaacagcat
     1681 atgttgcaac tcacagtctg gggcatcaag cagctccaag caagagtcct ggctatagaa
     1741 agatacctaa aagatcaaca gctcctaggg ctttggggct gctctggaaa actcatctgc
     1801 accactgctg taccttggaa ttccagttgg agtaataaat ctcaagcaga tatttgggat
     1861 aacatgacct ggatgcagtg ggatagagaa attagtaatt acacaggcat aatatacagt
     1921 ttgcttgaag aatcgcagaa ccagcaggaa aaaaatgaaa aagatttact agcattggat
     1981 agttggaaca gtctgtggaa ttggtttagc atatcaacat ggctgtggta tataagaata
     2041 ttcgtaataa tagtaggagg cttgataggt ttaagaataa tttttgctgt gctctcgcta
     2101 gtgaatagag ttaggcaggg atactcacct ttgtcatttc agacccttac cccaaacccg
     2161 agggaactcg acaggctcgg aggaatcgaa gaagaaggtg gagagcaaga cagaggcaga
     2221 tccatacgat tagtgaacgg attcttagca cttgtttggg acgacctgcg gagcctgtgc
     2281 ctcttcagct accaccactt gagagacttc acattgattg cagcgagagc agtggaactt
     2341 ctgggacgca gcattctcaa gggactacag agggggtggg aaatccttaa gtacctggga
     2401 agtcttgtgc agtcttgggg tctagagcta aaaaagagtg ctattagtct gcttgatacc
     2461 atagcaatag cagtagctga aggaacagat aggattatag aattcttaca aagaattgtt
     2521 agagctatcc tccatatacc tagaagaata agacagggct ttgaagcagc tttgcaataa
//
