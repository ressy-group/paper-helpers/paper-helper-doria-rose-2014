LOCUS       KF996597                2274 bp    RNA     linear   VRL 25-APR-2014
DEFINITION  HIV-1 isolate CAP256.5mo.20 from South Africa envelope glycoprotein
            (env) gene, complete cds.
ACCESSION   KF996597
VERSION     KF996597.1
KEYWORDS    .
SOURCE      Human immunodeficiency virus 1 (HIV-1)
  ORGANISM  Human immunodeficiency virus 1
            Viruses; Riboviria; Pararnavirae; Artverviricota; Revtraviricetes;
            Ortervirales; Retroviridae; Orthoretrovirinae; Lentivirus.
REFERENCE   1  (bases 1 to 2274)
  AUTHORS   Doria-Rose,N.A., Schramm,C.A., Gorman,J., Moore,P.L., Bhiman,J.N.,
            Dekosky,B.J., Ernandes,M.J., Georgiev,I.S., Kim,H.J., Pancera,M.,
            Staupe,R.P., Altae-Tran,H.R., Bailer,R.T., Crooks,E.T., Cupo,A.,
            Druz,A., Garrett,N.J., Hoi,K.H., Kong,R., Louder,M.K., Longo,N.S.,
            McKee,K., Nonyane,M., O'Dell,S., Roark,R.S., Rudicell,R.S.,
            Schmidt,S.D., Sheward,D.J., Soto,C., Wibmer,C.K., Yang,Y.,
            Zhang,Z., Mullikin,J.C., Binley,J.M., Sanders,R.W., Wilson,I.A.,
            Moore,J.P., Ward,A.B., Georgiou,G., Williamson,C., Abdool
            Karim,S.S., Morris,L., Kwong,P.D., Shapiro,L. and Mascola,J.R.
  CONSRTM   NISC Comparative Sequencing Program
  TITLE     Developmental pathway for potent V1V2-directed HIV-neutralizing
            antibodies
  JOURNAL   Nature 509, 55-62 (2014)
   PUBMED   24590074
REFERENCE   2  (bases 1 to 2274)
  AUTHORS   Moore,P.L., Bhiman,J.N., Williamson,C. and Sheward,D.J.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-DEC-2013) Centre for HIV and STIs, National Institute
            for Communicable Diseases, 1 Modderfontein Road, Sandringham,
            Johannesburg, Gauteng 2131, South Africa
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..2274
                     /organism="Human immunodeficiency virus 1"
                     /mol_type="genomic RNA"
                     /isolate="CAP256.5mo.20"
                     /isolation_source="23 weeks post-infection plasma sample"
                     /host="Homo sapiens; female"
                     /db_xref="taxon:11676"
                     /country="South Africa"
                     /collection_date="05-Jan-2006"
                     /note="subtype: C"
     gene            1..2274
                     /gene="env"
     CDS             1..2274
                     /gene="env"
                     /codon_start=1
                     /product="envelope glycoprotein"
                     /protein_id="AHJ58912.1"
                     /translation="MTVTGTWRNYQQWWIWGILGFWMLMICNGLWVTVYYGVPVWKEA
                     KTTLFCASDAKSYEKEVHNVWATHACVPTDPNPQELVLKNVTENFNMWKNDMVDQMHE
                     DIISLWDQSLKPCVKLTPLCVTLNCTTAKGINSTDNANATKKPNEEIKNCSLNTITEV
                     RDKQKKEYALFYRLDLVSLNEGDSEGNSSRSGNFSTYRLINCNTSVITQACPKVTFDP
                     IPIHYCAPAGYAILKCNNKTFNGTGPCNNVSTVQCTHGIKPVVSTQLLLNGSLAEEEI
                     IIRSENLTDNVKTIIVHLNESVEINCTRPNNNTRKSIRIGPGQTFYATGDIIGDIRQA
                     HCNISEIKWEKTLQRVSEKLREHFNKTIIFNQSSGGDLEITTHSFNCGGEFFYCNTSD
                     LFFNKTFNETYSTGSNSTNSTITLPCRIKQIINMWQEVGRAMYASPIAGEITCKSNIT
                     GLLLTRDGGGNNSTETETFRPGGGNMRDNWRSELYKYKVVEVKPLGIAPTNVYWNSSW
                     SNKTYNEIWDNMTWMQWDREIDNYTDTIYKLLEVSQKQQESNEKDLLALDSWNNLWNW
                     FDISKWLWYIKIFIMIVGGLIGLRIIFAVLSLVNRVRQGYSPLSFQTLTPNPRELDRL
                     GGIEEEGGEQDRDRSIRLVSGFXSLAWDDLRSLCLFCYHRLRDFILIAGRAVELLGRS
                     ILQGLQRGWEILKYLGSLVQYWGLELKKSAINLFDTLAIVVAEGTDRIIEFLQRIVRA
                     ILHIPRRIRQGFEAALQ"
ORIGIN      
        1 atgacagtga cggggacatg gaggaattat caacaatggt ggatatgggg aatcttaggc
       61 ttttggatgt taatgatttg taatggcttg tgggtcacag tctactatgg ggtacctgtg
      121 tggaaagaag caaaaactac tctattttgt gcctcagatg ctaaatcata tgagaaagag
      181 gtgcataatg tctgggctac acatgcctgt gtacccacag accccaaccc acaagaattg
      241 gttttgaaaa atgtaacaga aaattttaac atgtggaaaa atgayatggt agatcagatg
      301 catgaagata taatcagtyt atgggatcaa agcctcaagc catgtgtaaa attaacccca
      361 ctctgtgtca ctctaaattg tacaactgca aagggtatta atagtactga caatgcaaat
      421 gctactaaaa aaccaaatga agaaataaaa aattgctctc tcaatacaat cacagaggta
      481 agagataagc aaaagaaaga atatgcactc ttttatagac ttgatctagt atcacttaat
      541 gagggggact ctgaagggaa ctctagcaga tctggcaact ttagtacgta tagattaata
      601 aactgtaata cctcagtcat aacacaagcc tgtccaaagg tcacttttga cccaattcct
      661 atacattatt gtgctccagc tggttatgcg attctaaagt gtaataataa gacattcaat
      721 ggcacaggac catgcaataa tgtcagcaca gtacaatgta cacatggaat taagccagta
      781 gtttcaacwc aactattgtt aaatggtagc ctagcagaag aagagataat aattagatct
      841 gaaaacctga cagacaatgt caaaacaata atagtacatc tcaatgaatc tgtagagatt
      901 aattgtacaa gacccaacaa taatacaaga aaaagtataa gaataggacc aggacaaaca
      961 ttctatgcaa caggagacat aataggagat ataagacaag cacattgtaa cattagtgaa
     1021 attaaatggg agaaaacttt acaaagagta agtgaaaaat tgagagaaca cttcaataag
     1081 acaataatat ttaatcaatc ctcaggaggg gacctagaaa ttacaacaca tagctttaat
     1141 tgtggaggag aatttttcta ttgcaataca tcagatctgt tttttaataa gacatttaat
     1201 gagacatata gtacaggaag taattcaaca aattcaacca tcacactccc atgcagaata
     1261 aagcaaatta taaacatgtg gcaggaggtg ggtcgagcaa tgtatgcctc tcctattgca
     1321 ggagaaataa catgtaaatc aaatatcaca ggactactat tgacacgtga tggaggagga
     1381 aacaacagta cagagacaga gacattcaga cctggaggag gaaatatgag ggacaactgg
     1441 agaagtgaat tatataaata taaagtggta gaagttaagc cattaggaat agcacccact
     1501 aatgtgtatt ggaactctag ttggagtaat aaaacttaca atgaaatttg ggataacatg
     1561 acatggatgc agtgggatag agaaattgat aactacacag acacaatata caagctgctt
     1621 gaagtctcgc aaaaacagca ggagagcaat gaaaaagatt tactagcatt ggacagttgg
     1681 aacaatctgt ggaattggtt tgacatatca aaatggctgt ggtatataaa aatattcata
     1741 atgatagtag gaggcttgat aggcttaaga ataatttttg ctgtgctctc gctagtgaat
     1801 agagttaggc agggatactc acctttgtca tttcagaccc ttaccccaaa cccgagggaa
     1861 ctcgacaggc tcggaggaat cgaagaagaa ggtggagagc aagacagaga cagatccata
     1921 agattagtga gcggattctt mtcacttgcc tgggacgacc tgcggagcct gtgcctcttc
     1981 tgctaccacc gattgagaga cttcatatta attgcaggga gagcagtgga acttctggga
     2041 cgcagcattc tccagggact acaaaggggt tgggaaatcc ttaagtatct gggaagtctt
     2101 gtgcagtatt ggggtctgga gctaaaaaag agtgctatta atctgtttga taccttagca
     2161 atagtagtag ctgaaggaac agatagaatt atagaattct tacaaagaat tgttagagct
     2221 atcctccaca tacctagaag aataagacag ggctttgaag cagctttgca ataa
//
