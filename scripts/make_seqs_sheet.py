#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import re
import sys
from csv import DictReader, DictWriter
from Bio import SeqIO

ACC_MAP = dict(
    [(f"KF{x}", "env")         for x in range(996576, 996717)] + \
    [(f"KJ{x}", "ngs_heavy")   for x in range(133708, 134388)] + \
    [(f"KJ{x}", "ngs_light")   for x in range(134388, 134860)] + \
    [(f"KJ{x}", "mabs_or_inf") for x in range(134860, 134890)])

def parse_seq_desc_ngs(txt, category):
    # Example for NGS heavy and light:
    # KJ134334.1 Homo sapiens anti-HIV-1 immunoglobulin cap256-119-182676 chain variable region mRNA, partial cds
    # KJ134559.1 Homo sapiens isolate 048-170705 anti-HIV-1 immunoglobulin 048-170705 chain variable region mRNA, partial cds
    # (I think they must have left out the actual "heavy" and "light" before "... chain variable region.")
    if category == "ngs_heavy":
        pat = r"([A-Z0-9.]+) Homo sapiens anti-HIV-1 " \
            r"immunoglobulin cap256-([0-9]+)-([0-9]+) chain variable region mRNA, partial cds"
        match = re.match(pat, txt)
        fields = {"Chain": "heavy"}
        fields.update(dict(zip(
            ["Accession", "Timepoint", "ClusterID"],
            match.groups())))
    else:
        pat = r"([A-Z0-9.]+) Homo sapiens isolate ([0-9]+)-([0-9]+) anti-HIV-1 " \
            r"immunoglobulin ([0-9]+)-([0-9]+) chain variable region mRNA, partial cds"
        match = re.match(pat, txt)
        fields = {"Chain": "light"}
        grps = match.groups()
        if not (grps[1] == grps[3] and grps[2] == grps[4]):
            # just making sure the ID always matches up as it looks like
            raise ValueError
        fields.update(dict(zip(
            ["Accession", "Timepoint", "ClusterID"],
            grps[0:3])))
    suffix = "H" if fields["Chain"] == "heavy" else "L"
    fields["SeqID"] = f"CAP256-{fields['Timepoint']}-{fields['ClusterID']}-{suffix}"
    fields["Timepoint"] = int(fields["Timepoint"])
    del fields["ClusterID"]
    fields["Category"] = "NGS"
    fields["SeqType"] = "Observed"
    return fields

def parse_seq_desc_inf(txt):
    # Examples for mAbs/inferred seqs:
    #KJ134860.1 Homo sapiens anti-HIV-1 immunoglobulin CAP256-VRC26.UCA heavy chain variable region mRNA, partial cds
    #KJ134861.1 Homo sapiens anti-HIV-1 immunoglobulin CAP256-VRC26.I1 heavy chain variable region mRNA, partial cds
    #KJ134864.2 Homo sapiens anti-HIV-1 immunoglobulin CAP256-VRC26.I1 light chain variable region mRNA, partial cds
    #KJ134866.1 Homo sapiens anti-HIV-1 immunoglobulin CAP256-VRC26.01 heavy chain variable region mRNA, partial cds
    pat = r"([A-Z0-9.]+) Homo sapiens anti-HIV-1 immunoglobulin " \
        r"(CAP256-VRC26\.[A-Z0-9]+) (heavy|light) chain variable region mRNA, partial cds"
    match = re.match(pat, txt)
    fields = dict(zip(
        ["Accession", "SeqID", "Chain"],
        match.groups()))
    suffix = re.sub(r".*\.", "", fields["SeqID"])
    if "UCA" in suffix or "I" in suffix:
        fields["Category"] = "Inferred"
        fields["SeqType"] = "Inferred"
    else:
        fields["Category"] = "mAb"
        fields["SeqType"] = "Observed"
    suffix = "H" if fields["Chain"] == "heavy" else "L"
    fields["SeqID"] += f"-{suffix}"
    return fields

FIELDS = [
    "SeqID",
    "Category",
    "SeqType",
    "Timepoint",
    "Chain",
    "Accession",
    "Seq"]

def make_seqs_sheet(fastas):
    antibody_attrs = {}
    with open("from-paper/fig1b.csv") as f_in:
        for row in DictReader(f_in):
            antibody_attrs[row["Antibody"]] = row
    all_attrs = []
    for fasta in fastas:
        with open(fasta) as f_in:
            for record in SeqIO.parse(f_in, "fasta"):
                category = ACC_MAP[record.id.split(".")[0]]
                if category == "env":
                    continue
                if category in ["ngs_heavy", "ngs_light"]:
                    fields = parse_seq_desc_ngs(record.description, category)
                elif category == "mabs_or_inf":
                    fields = parse_seq_desc_inf(record.description)
                else:
                    raise ValueError
                fields["Seq"] = str(record.seq)
                # The GenBank records don't report timepoints per numbered mAb,
                # but Figure 1B does
                if fields["Category"] == "mAb":
                    key = re.sub(r"-[HKL]$", "", fields["SeqID"])
                    fields["Timepoint"] = int(antibody_attrs[key]["Week"])
                all_attrs.append(fields)
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(all_attrs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
