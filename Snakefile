from csv import DictReader

wildcard_constraints:
    srr="SRR[0-9]+"

SHEETS_URL = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQbJYClk7rPweq7evSsEsG2WrgrZESs3sUjKwoZuk526XVLFgJyzXfLZOs7iGylX2-QUxDNmTYB1zzF/pub"
SHEETS_GIDS = {
    "fig1b": "948684383",
    "fig3a": "683420295",
    "tableS1": "1237636852",
    "tableS4": "2013835901",
    "tableS5": "883876185"}

with open("from-paper/genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]

SRA = []
for srp in ["SRP017087", "SRP034555"]:
    with open(f"SraRunTable_{srp}.txt") as f_in:
        SRA.extend(list(DictReader(f_in)))

TARGET_GBF = expand("from-genbank/{acc}.gbf", acc=GB_ACCESSIONS)
TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)
TARGET_PAPER_SHEETS = expand("from-paper/{sheet}.csv", sheet=SHEETS_GIDS.keys())
TARGET_OUTPUT_SHEETS = expand("output/{sheet}.csv", sheet=["seqs"])
TARGET_SRA_FASTQ_PAIRED = expand("from-sra/{srr}_{rp}.fastq.gz", srr=[row["Run"] for row in SRA if "ILLUMINA" == row["Platform"]], rp=[1, 2])
TARGET_SRA_FASTQ_SINGLE = expand("from-sra/{srr}.fastq.gz", srr=[row["Run"] for row in SRA if "LS454" == row["Platform"]])

rule all:
    input: TARGET_PAPER_SHEETS + TARGET_OUTPUT_SHEETS + TARGET_GBF + TARGET_GBF_FASTA + TARGET_SRA_FASTQ_PAIRED + TARGET_SRA_FASTQ_SINGLE

rule all_gbf:
    input: TARGET_GBF

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule all_output_sheets:
    input: TARGET_OUTPUT_SHEETS

rule all_paper_sheets:
    input: TARGET_PAPER_SHEETS

rule all_sra_fastq:
    input: TARGET_SRA_FASTQ_PAIRED + TARGET_SRA_FASTQ_SINGLE

rule sra_fastq_split:
    output: expand("from-sra/{{srr}}_{rp}.fastq.gz", rp=[1, 2])
    shell: "fastq-dump --split-files --gzip {wildcards.srr} --outdir from-sra"

rule sra_fastq_single:
    output: "from-sra/{srr}.fastq.gz"
    shell: "fastq-dump --gzip {wildcards.srr} --outdir from-sra"

rule make_seqs_sheet:
    output: "output/seqs.csv"
    input:
        fastas=TARGET_GBF_FASTA,
        fig1b="from-paper/fig1b.csv"
    shell: "python scripts/make_seqs_sheet.py {input.fastas} > {output}"

rule download_gbf:
    """Download one GBF text file per GenBank accession."""
    output: "from-genbank/{acc}.gbf"
    shell: "python scripts/download_genbank.py {wildcards.acc} gb > {output}"

rule download_gbf_fa:
    """Download one FASTA file per GenBank accession."""
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_genbank.py {wildcards.acc} fasta > {output}"

rule download_sheet:
    output: "from-paper/{sheet}.csv"
    params:
        url=SHEETS_URL,
        gid=lambda w: SHEETS_GIDS[w.sheet]
    shell:
        """
            curl -L '{params.url}?gid={params.gid}&single=true&output=csv' > {output}
            dos2unix {output}
            echo >> {output}
        """
